=====Pre Instalasi=====

# Membuat Directory Kerja
mkdir BoT
# Masuk pada Directory Kerja
cd BoT
# clone Source Code=====
git clone https://gitlab.com/ar0nt/bothlf2xtraining.git
=====Cek Hasil Clonning=====
ls
# Jika Terdapat directory bothlf2xtraining, maka proses clonning telah berhasil dilakukan

# Penginstalan Lingkungan Pengembangan
# Masuk pada Directory bothlf2xtraining untuk memulai inisialisasi
cd bothlf2xtraining
# Install prerequested
## Memberikan Hak Akses untuk melakukan eksekusi file init.sh
sudo chmod +x init.sh
# melakukan eksekusi file init.sh
./init.sh

# Cek Lingkungan Pengembangan
## pengecekkan version node dan npm
node --version 
### versi yang digunakan adalah v12.xx.x
npm --version 
### versi yang digunakan adalah v6.xx.x
## Pengecekkan Versi dari docker
docker --version 
### versi yang digunakan adalah v20.10.x
## Pengecekkan docker berjalan dengan baik
docker ps
## jika proses inisialisasi berhasil maka docker akan memberikan daftar container yang sedang berjalan. Saat ini, tidak ada container yang sedang berjalan
## jika anda mendapatkan permasalahan hak untuk melakukan eksekusi (permission problem) maka lakukan setting hak eksekusi pada docker dengan menjalan 2 perintah dibahawah ini
    sudo usermod -aG docker $USER
    sudo chmod 666 /var/run/docker.sock
## jika anda mendapatkan error yang lain, silahkan menghubungi asisten untuk dibantu
=====END Of Pre Instalasi=====

=====Instalasi=====
# Download dan Instalasi Hyperledger Fabric 2.x (HLF2)
## command ini akan melakukkan download sekaligus melakukan instalasi HLF2
./installFabric.sh
## command ini akan melakukkan Setup Environment Variable dari HLF2
./setupFabricEnvironment.sh

# Memastikan Proses Instalasi dan Konfigurasi HLF2 Berjalan dengan baik
peer version
## jika proses Instalasi dan Konfigurasi HLF2 berhasil, maka Peer Version yang akan ditampilkan adalah 2.3.x
## jika anda mendapatkan error yang lain, silahkan menghubungi asisten untuk dibantu
=====END Of Instalasi=====

=====Instalasi dan Konfigurasi Fabric Chaincode Sample=====
# masuk ke directory BoT/App/chainFabcar
cd $HOME/BoT/App/chainFabcar
# install chainFabcar
npm install
# Pindah directory ke BoT/core/network
cd ../../network
# build chainFabcar menjadi paket chaincode
peer lifecycle chaincode package basic.tar.gz \
      --path ../App/chainFabcar \
      --lang node \
      --label basic_1.0
# proses diatas akan membuat file basic.tar.gz pada directory core/network
# Instalasi Paket Chaincode pada Blockchain
## Jalankan Script Environment Variable
source ./scripts/envVar.sh 
## Setup Environment Sebagai Organization 1
setGlobals 1
## Setup Parameter Sertifikat Node Orderer
export ORDERER_CERTFILE=$PWD/organizations/ordererOrganizations/example.com/orderers/orderer.example.com/msp/tlscacerts/tlsca.example.com-cert.pem
## Setup Parameter Sertifikat Node Peer Untuk Organization 1
export ORG1_PEER_CERTFILE=$PWD/organizations/peerOrganizations/org1.example.com/peers/peer0.org1.example.com/tls/ca.crt
## Setup Parameter Sertifikat Node Peer Untuk Organization 2
export ORG2_PEER_CERTFILE=$PWD/organizations/peerOrganizations/org2.example.com/peers/peer0.org2.example.com/tls/ca.crt
# Proses Instalasi Chaincode Pada Environment Organization 1
setGlobals 1
peer lifecycle chaincode install basic.tar.gz
## Proses diatas akan menghasilkan Kode Paket, silahkan copykan Paket ID tersebut pada sebuah file

# Proses Instalasi Chaincode Pada Environment Organization 2
setGlobals 2
peer lifecycle chaincode install basic.tar.gz

# Setup Paket ID
# Silahkan Ganti Paket ID yang didapatkan pada proses sebelumnya
## Contoh Paket ID =basic_1.0-94c84bb2cc404bab2d945d62dc8d8d3837e2074966495d037b27ecfdf7fe171a
export PKGID=SILAHKAN_PASTEKAN_PAKET_ID_Anda_disini

# Proses Persetujuan terhadap Chaincode yang telah terinstall
## Approval Oleh Organization 1
setGlobals 1
peer lifecycle chaincode approveformyorg \
      -o localhost:7050 \
      --ordererTLSHostnameOverride  orderer.example.com \
      --tls --cafile $ORDERER_CERTFILE  \
      --channelID channel1 \
      --name basic \
      --version 1 \
      --package-id $PKGID \
      --sequence 1 
## Approval Oleh Organization 2
setGlobals 2
peer lifecycle chaincode approveformyorg \
      -o localhost:7050 \
      --ordererTLSHostnameOverride  orderer.example.com \
      --tls --cafile $ORDERER_CERTFILE  \
      --channelID channel1 \
      --name basic \
      --version 1 \
      --package-id $PKGID \
      --sequence 1
## Proses Commit chaincode
### Proses Ini dilakukan untuk melakukan posting/publikasi perjanjian penggunaan chaincode oleh Organization 1 dan Organization 2, Posting/Publikasi dilakukan dengan memasangkan chaincode pada Blockchain
peer lifecycle chaincode commit \
      -o localhost:7050 \
      --ordererTLSHostnameOverride orderer.example.com \
      --tls --cafile $ORDERER_CERTFILE \
      --channelID channel1 --name basic \
      --peerAddresses localhost:7051 --tlsRootCertFiles $ORG1_PEER_CERTFILE \
      --peerAddresses localhost:9051 --tlsRootCertFiles $ORG2_PEER_CERTFILE \
      --version 1 --sequence 1 
## Melihat kembali perjanjian yang telah dipublikasi pada blockcchain
peer lifecycle chaincode querycommitted \
      --channelID channel1 --name basic \
      --cafile  $ORDERER_CERTFILE
## Perjanjian yang telah dicommit oleh para members akan tampak pada docker sebagai sebuah virtual mesin
docker ps
=====END Of Instalasi dan Konfigurasi Fabric Chaincode Sample=====

=====Operasi Fabric Chaincode Sample Menggunakan CLI=====
# Inisiasi Chaincode dengan memanggil fungsi InitLedger pada Chaincode
peer chaincode invoke \
      -o localhost:7050 --ordererTLSHostnameOverride orderer.example.com \
      --tls --cafile $ORDERER_CERTFILE  \
      -C channel1 -n basic \
      --peerAddresses localhost:7051 --tlsRootCertFiles $ORG1_PEER_CERTFILE \
      --peerAddresses localhost:9051 --tlsRootCertFiles $ORG2_PEER_CERTFILE \
      -c '{"function": "InitLedger", "Args": []}'
# view All Asset pada Ledger
peer chaincode query -C channel1 -n basic -c '{"Args":["GetAllAssets"]}' | jq 
# Membaca Asset yang spesifik
peer chaincode query -C channel1 -n basic -c '{"Args":["ReadAsset", "asset6"]}' | jq 

=====END Of Operasi Fabric Chaincode Sample Menggunakan CLI=====