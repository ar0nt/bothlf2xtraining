mkdir ../fabric 
cd ../fabric 
echo "================Mulai Download Fabric Samples========================"
curl -sSL https://bit.ly/2ysbOFE | bash -s -- 2.3.1 1.4.7
echo "================Selesai Download Fabric Samples======================"
echo ""
echo "================Mulai Membuat Channel channel1======================="
# cd fabric-samples/test-network
#./network.sh up createChannel -c channel1 -ca

echo "================Setup Chaincode Environment================"
mkdir ../bothlf2xtraining/core
cd ../bothlf2xtraining/core
cp -R ../../fabric/fabric-samples/bin .
cp -R ../../fabric/fabric-samples/config .
cp -R ../../fabric/fabric-samples/test-network .
mv test-network/ network
echo ""
echo "================Mulai Membuat Channel channel1======================="
cd network
./network.sh up createChannel -c channel1 -ca
echo "================Selesai Membuat Channel channel1======================="


echo "================Selesai Setup Chaincode Environment================"