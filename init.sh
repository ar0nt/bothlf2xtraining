#!/bin/bash
#memberikan hak akses untuk melakukan eksekusi file installNode.sh
sudo chmod +x installNode.sh
#memberikan hak akses untuk melakukan eksekusi file installFabric.sh
sudo chmod +x installFabric.sh
#memberikan hak akses untuk melakukan eksekusi file setup_chaincode_env.sh
sudo chmod +x setup_chaincode_env.sh
#memberikan hak akses untuk melakukan eksekusi file setupFabricEnvironment.sh
sudo chmod +x setupFabricEnvironment.sh

echo "========= Mulai Proses Inisialisasi =========== "
#menginstall curl dan jq
sudo apt install curl jq 
#menginstall docker-compose
sudo apt-get -y install docker-compose
docker --version 
#Memulai Docker
sudo systemctl start docker
sudo systemctl enable docker 
sudo usermod -aG docker $USER
sudo chmod 666 /var/run/docker.sock
#menginstall NodeJss
./installNode.sh
echo "========= Selesai Proses Inisialisasi  =========== "