cd $HOME/BoT/bothlf2xtraining/core/bin 
# setup core/config sebagai Fabric Config Path
export PATH=$PATH:$PWD
# setup core/bin sebagai Fabric Binnaries Path
cd ../config
export FABRIC_CFG_PATH=$PWD
echo $FABRIC_CFG_PATH 
# setup Fabric Environment Variables
cd ../network
source ./scripts/envVar.sh